package md56534843254f3328189acf6b9126683ad;


public abstract class MvxDialogFragment
	extends md553e774b876928d8d6c198058ecf381ac.MvxEventSourceDialogFragment
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("MvvmCross.Droid.Support.V7.Fragging.Fragments.MvxDialogFragment, MvvmCross.Droid.Support.V7.Fragging", MvxDialogFragment.class, __md_methods);
	}


	public MvxDialogFragment ()
	{
		super ();
		if (getClass () == MvxDialogFragment.class)
			mono.android.TypeManager.Activate ("MvvmCross.Droid.Support.V7.Fragging.Fragments.MvxDialogFragment, MvvmCross.Droid.Support.V7.Fragging", "", this, new java.lang.Object[] {  });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
