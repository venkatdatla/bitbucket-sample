package md59eaff91192b861f3e640824432d39d08;


public class MvxFragmentActivity
	extends md59eaff91192b861f3e640824432d39d08.MvxEventSourceFragmentActivity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_setContentView:(I)V:GetSetContentView_IHandler\n" +
			"n_attachBaseContext:(Landroid/content/Context;)V:GetAttachBaseContext_Landroid_content_Context_Handler\n" +
			"";
		mono.android.Runtime.register ("MvvmCross.Droid.Support.V7.Fragging.MvxFragmentActivity, MvvmCross.Droid.Support.V7.Fragging", MvxFragmentActivity.class, __md_methods);
	}


	public MvxFragmentActivity ()
	{
		super ();
		if (getClass () == MvxFragmentActivity.class)
			mono.android.TypeManager.Activate ("MvvmCross.Droid.Support.V7.Fragging.MvxFragmentActivity, MvvmCross.Droid.Support.V7.Fragging", "", this, new java.lang.Object[] {  });
	}


	public void setContentView (int p0)
	{
		n_setContentView (p0);
	}

	private native void n_setContentView (int p0);


	public void attachBaseContext (android.content.Context p0)
	{
		n_attachBaseContext (p0);
	}

	private native void n_attachBaseContext (android.content.Context p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
