package md5574c1700f54c1af9f21719cde6491b73;


public abstract class MvxActivity
	extends md53f3bfe27c5dcc9fb9198fe96aab01e42.MvxEventSourceActivity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_setContentView:(I)V:GetSetContentView_IHandler\n" +
			"n_attachBaseContext:(Landroid/content/Context;)V:GetAttachBaseContext_Landroid_content_Context_Handler\n" +
			"";
		mono.android.Runtime.register ("MvvmCross.Droid.Views.MvxActivity, MvvmCross.Droid", MvxActivity.class, __md_methods);
	}


	public MvxActivity ()
	{
		super ();
		if (getClass () == MvxActivity.class)
			mono.android.TypeManager.Activate ("MvvmCross.Droid.Views.MvxActivity, MvvmCross.Droid", "", this, new java.lang.Object[] {  });
	}


	public void setContentView (int p0)
	{
		n_setContentView (p0);
	}

	private native void n_setContentView (int p0);


	public void attachBaseContext (android.content.Context p0)
	{
		n_attachBaseContext (p0);
	}

	private native void n_attachBaseContext (android.content.Context p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
